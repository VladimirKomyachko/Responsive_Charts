
function draw_rav_chart(hist_data) {
    var ctx = $("#ravChart");

    var labels = [];
    var data = [];
    var colors = [];
    var max_cnt = 0;
    hist_data.forEach(function(element) {
        max_cnt = Math.max(max_cnt, element.cnt);
    });

    hist_data.forEach(function(element) {
        if (element.bucket != 1) {
            labels.push(element.bucket);
            data.push(element.cnt);
            colors.push(colorbrewer['Paired'][9][3 + Math.floor(element.cnt/(max_cnt+0.01) * 6)])
        }
    });

    var data = {
        labels: labels,
        datasets: [
            {
                data: data,
                backgroundColor: colors
            }]
    };

    var options = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Count records'
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Rav/City avg.'
                }
            }]
        },
        legend : {
            display: false
        }
    };

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });

}

function draw_sfp_chart(hist_data) {
    var ctx = $("#sfpChart");

    var labels = [];
    var data = [];
    var colors = [];

    hist_data.forEach(function (element, index) {
        labels.push(element.bucket);
        data.push(element.cnt);
        colors.push(colorbrewer['Set1'][5][1+index]);
    });

    var data = {
        labels: labels,
        datasets: [
            {
                data: data,
                backgroundColor: colors
            }]
    };

    var options = {
        responsive: true,
        legend:
        {
            position: "right"
        }
    };

    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        options: options
    });

}

function draw_friends_chart(hist_data) {
    var ctx = $("#friendsChart");

    var labels = [];
    var data = [];

    hist_data.some(function (element) {
        if (element.y > 0) {
            data.push(element);
        }
        if (element.y > 1000) {
            element.y = 1000;
        }


        if (data.length > 10000)
            return true;
    });

    var data = {
        labels: labels,
        datasets: [
            {
                data: data,
                backgroundColor: colorbrewer['BuPu'][9][6],
                showLine: false
            }]
    };

    var options = {
        responsive: true,
        scales: {
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Age'
                }
            }],
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Friends'
                }
            }]
        },
        legend : {
            display: false
        }

    };

    var myChart = new Chart(ctx, {
        type: 'scatter',
        data: data,
        options: options
    });

}

function draw_enrichment_chart(hist_data) {
    var ctx = $("#enrichmentChart");

    var labels = [];
    var data = [];

    var MONTHS_TO_SHOW = 13;
    hist_data.slice(-MONTHS_TO_SHOW).some(function (element) {
        data.push(element.cnt_records);
        labels.push(element.relevant_date)
    });

    var data = {
        labels: labels,
        datasets: [
            {
                data: data,
                backgroundColor: colorbrewer['BrBG'][9][6]
            }]
    };

    var options = {
        legend : {
            display: false
        }
    };

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });

}

function draw_general_chart(hist_data) {
    var ctx = $("#generalChart");

    var labels = [];
    var data = [];
    hist_data.forEach(function (element) {
        data.push(element.pct);
        labels.push(element.key)
    });

    var color = colorbrewer['Spectral'][9][6];
    var data = {
        labels: labels,
        datasets: [
            {
                data: data,
                borderColor: color,
                pointColor: color,
                pointBackgroundColor: color
            }]
    };

    var options = {
        legend : {
            display: false
        },
        scale: {
            ticks: {
                beginAtZero: true
            }
        }
    };

    var myChart = new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    });

}


function populate_rav_chart()
{
    $.post('/records/get_rav_stats/')
        .always(function(data) {
            //var hist_data = $.parseJSON(data);
            var hist_data = [{"bucket": "0.00-0.75", "cnt": 98}, {"bucket": "0.75-1.00", "cnt": 577}, {"bucket": "1.00-1.25", "cnt": 369}, {"bucket": "1.25-1.50", "cnt": 60}, {"bucket": "1.50-up", "cnt": 64}];
            draw_rav_chart(hist_data);
        });
}

function populate_sfp_chart()
{
    $.post('/records/get_sfp_stats/')
        .always(function(data) {
            //var hist_data = $.parseJSON(data);
            var hist_data = [{"bucket": "0-25", "cnt": 371}, {"bucket": "25-50", "cnt": 762}, {"bucket": "50-75", "cnt": 135}, {"bucket": "75-100", "cnt": 39}];
            draw_sfp_chart(hist_data);
        });
}

function populate_friends_chart()
{
    $.post('/records/get_friends_stats/')
        .always(function(data) {
            //var hist_data = $.parseJSON(data);
            var hist_data = [{"y": "567", "x": 42}, {"y": "358", "x": 42}, {"y": "761", "x": 42}, {"y": "525", "x": 42}, {"y": "245", "x": 42}, {"y": "0", "x": 42}, {"y": "225", "x": 42}, {"y": "181", "x": 42}, {"y": "287", "x": 42}, {"y": "0", "x": 65}, {"y": "0", "x": 42}, {"y": "536", "x": 31}, {"y": "0", "x": 65}, {"y": "0", "x": 31}, {"y": "0", "x": 60}, {"y": "0", "x": 35}, {"y": "0", "x": 60}, {"y": "0", "x": 38}, {"y": "0", "x": 42}, {"y": "0", "x": 44}, {"y": "0", "x": 56}, {"y": "0", "x": 52}, {"y": "0", "x": 37}, {"y": "0", "x": 49}, {"y": "0", "x": 47}, {"y": "0", "x": 38}, {"y": "0", "x": 27}, {"y": "0", "x": 44}, {"y": "0", "x": 44}, {"y": "0", "x": 55}, {"y": "0", "x": 61}, {"y": "299", "x": 37}, {"y": "0", "x": 27}, {"y": "0", "x": 61}, {"y": "0", "x": 44}, {"y": "0", "x": 38}, {"y": "0", "x": 47}, {"y": "73", "x": 43}, {"y": "167", "x": 55}, {"y": "258", "x": 49}, {"y": "571", "x": 44}, {"y": "573", "x": 67}, {"y": "562", "x": 50}, {"y": "138", "x": 46}, {"y": "0", "x": 32}, {"y": "282", "x": 40}, {"y": "36", "x": 66}, {"y": "0", "x": 66}, {"y": "1097", "x": 30}, {"y": "166", "x": 39}, {"y": "516", "x": 43}, {"y": "434", "x": 36}, {"y": "556", "x": 47}, {"y": "0", "x": 44}, {"y": "0", "x": 41}, {"y": "178", "x": 34}, {"y": "0", "x": 55}, {"y": "315", "x": 35}, {"y": "280", "x": 35}, {"y": "0", "x": 53}, {"y": "1250", "x": 34}, {"y": "80", "x": 35}, {"y": "217", "x": 51}, {"y": "348", "x": 37}, {"y": "103", "x": 49}, {"y": "0", "x": 39},
{"y": "0", "x": 52}, {"y": "0", "x": 33}, {"y": "124", "x": 36}, {"y": "396", "x": 42}, {"y": "766", "x": 44}, {"y": "0", "x": 35}, {"y": "1", "x": 41}, {"y": "467", "x": 58}, {"y": "127", "x": 43}, {"y": "122", "x": 60}, {"y": "0", "x": 46}, {"y": "1388", "x": 27}, {"y": "512", "x": 43}, {"y": "17", "x": 49}, {"y": "0", "x": 53}, {"y": "49", "x": 45}, {"y": "640", "x": 67}, {"y": "710", "x": 31}, {"y": "292", "x": 42}, {"y": "820", "x": 28}, {"y": "1162", "x": 36}, {"y": "336", "x": 49}, {"y": "0", "x": 34}, {"y": "125", "x": 43}, {"y": "1121", "x": 35}, {"y": "254", "x": 50}, {"y": "293", "x": 42}, {"y": "0", "x": 52}, {"y": "118", "x": 58}, {"y": "0", "x": 37}, {"y": "86", "x": 44}, {"y": "0", "x": 31}, {"y": "330", "x": 51}, {"y": "712", "x": 56}, {"y": "0", "x": 53}, {"y": "250", "x": 33}, {"y": "8", "x": 59}, {"y": "3478", "x": 65}, {"y": "520", "x": 41}, {"y": "539", "x": 46}, {"y": "0", "x": 33}, {"y": "374", "x": 36}, {"y": "124", "x": 39}, {"y": "811", "x": 30}, {"y": "1484", "x": 62}, {"y": "902", "x": 37}, {"y": "74", "x": 66}, {"y": "427", "x": 60}, {"y": "340", "x": 30}, {"y": "0", "x": 32}, {"y": "0", "x": 38}, {"y": "0", "x": 40}, {"y": "684", "x": 44}, {"y": "0", "x": 53}, {"y": "203", "x": 36}, {"y": "320", "x": 53}, {"y": "0", "x": 44}, {"y": "0", "x": 28}, {"y": "738", "x": 33}, {"y": "0", "x": 52}, {"y": "536", "x": 31}, {"y": "17", "x": 64}, {"y": "157", "x": 38}, {"y": "221", "x": 33}, {"y": "0", "x": 34}, {"y": "606", "x": 29}, {"y": "433", "x": 37}, {"y": "320", "x": 29}, {"y": "910", "x": 68}, {"y": "0", "x": 35}, {"y": "242", "x": 42}, {"y": "0", "x": 64}, {"y": "2196", "x": 52}, {"y": "534", "x": 31}, {"y": "137", "x": 44}, {"y": "171", "x": 53}, {"y": "0", "x": 40}, {"y": "295", "x": 38}, {"y": "0", "x": 66}, {"y": "387", "x": 44}, {"y": "0", "x": 43}, {"y": "0", "x": 32}, {"y": "0", "x": 46}, {"y": "0", "x": 48}, {"y": "227", "x": 71}, {"y": "0", "x": 48}, {"y": "36", "x": 34}, {"y": "98", "x": 43}, {"y": "270", "x": 34}, {"y": "419", "x": 42}, {"y": "0", "x": 38}, {"y": "0", "x": 55}, {"y": "29", "x": 27}, {"y": "0", "x": 41}, {"y": "102", "x": 39}, {"y": "748", "x": 46}, {"y": "0", "x": 44}, {"y": "0", "x": 43}, {"y": "54", "x": 39}, {"y": "0", "x": 35}, {"y": "109", "x": 65}, {"y": "243", "x": 48}, {"y": "0", "x": 30}, {"y": "0", "x": 32}, {"y": "623", "x": 38}, {"y": "686", "x": 43}, {"y": "296", "x": 38}];
            draw_friends_chart(hist_data);
        });
}

function populate_enrichment_chart()
{
    $.post('/records/get_enrichment_stats/')
        .always(function(data) {
            //var hist_data = $.parseJSON(data);
            var hist_data = [
              {"relevant_date": "2016-04", "cnt_records": 0},
               {"relevant_date": "2016-05", "cnt_records": 0},
                {"relevant_date": "2016-06", "cnt_records": 0},
                 {"relevant_date": "2016-07", "cnt_records": 330},
                  {"relevant_date": "2016-08", "cnt_records": 122},
                   {"relevant_date": "2016-09", "cnt_records": 211},
                    {"relevant_date": "2016-10", "cnt_records": 0},
                     {"relevant_date": "2016-11", "cnt_records": 111},
                      {"relevant_date": "2016-12", "cnt_records": 222},
                       {"relevant_date": "2017-01", "cnt_records": 666}, {"relevant_date": "2017-02", "cnt_records": 222}, {"relevant_date": "2017-03", "cnt_records": 111}, {"relevant_date": "2017-04", "cnt_records": 0}, {"relevant_date": "2017-05", "cnt_records": 703}, {"relevant_date": "2017-06", "cnt_records": 807}, {"relevant_date": "2017-07", "cnt_records": 0}];
            draw_enrichment_chart(hist_data);
        });
}

function populate_general_chart()
{
    $.post('/records/get_general_stats/')
        .always(function(data) {
            //var hist_data = $.parseJSON(data);
            var hist_data = [{"key": "bankrupt", "pct": 58.2}, {"key": "found_fb", "pct": 83.7}, {"key": "found_pages", "pct": 57.9}, {"key": "found_rav", "pct": 73.7}, {"key": "self_proprietor", "pct": 64.0}];
            draw_general_chart(hist_data);
        });
}

$(document).ready(function() {
    populate_sfp_chart();
    populate_friends_chart();
    populate_enrichment_chart();
    populate_rav_chart();
    populate_general_chart();
});
